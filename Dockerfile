FROM python:3.8-slim

RUN export DEBIAN_FRONTEND=noninteractive && apt-get update
RUN apt-get -y install tesseract-ocr \
    tesseract-ocr-eng \
    tesseract-ocr-nld \
    tesseract-ocr-fra \
    tesseract-ocr-deu \
    tesseract-ocr-rus
# RUN apt install -y libsm6 libxext6

WORKDIR /flask
COPY requirements.txt /flask/requirements.txt
RUN pip3 install -r requirements.txt
COPY . /flask

CMD ["python", "app.py"]