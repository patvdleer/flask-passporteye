#!/usr/bin/env python3
import io
import os
import logging
from flask import Flask, request, make_response, jsonify, render_template
from pytesseract import TesseractError
from werkzeug.utils import secure_filename
from passporteye.mrz.image import MRZPipeline
from passporteye import read_mrz

try:
    from PIL import Image
except ImportError:
    import Image

import re

app = Flask(__name__)

log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=log_fmt)


@app.route('/', methods=['GET'])
def process_get():
    return render_template('index.html')


@app.route('/', methods=['POST'])
def process_post():
    image = request.files.get('file', None)
    if not image:
        return make_response("Missing file parameter", 400)

    fn = f"/tmp/{secure_filename(image.filename)}"
    image.save(fn)

    try:
        data = read_mrz(fn, save_roi=False, extra_cmdline_params='--oem 0')
    except TesseractError:
        data = read_mrz(fn, save_roi=False)

    if data is None:
        os.remove(fn)
        return make_response("Can not read image", 400)

    mrz_data = data.to_dict()
    os.remove(fn)

    return jsonify(mrz_data)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=False)
